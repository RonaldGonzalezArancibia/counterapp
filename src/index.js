import React from 'react';
import ReactDOM from 'react-dom';
import PrimeraApp from './PrimeraApp';
import CounterApp from './CounterApp';
import './index.css'
  

const divRoot = document.querySelector('#root');

ReactDOM.render(<CounterApp value={123}/>,divRoot);
// ReactDOM.render(<PrimeraApp saludo='hola soy goku'/>,divRoot);

