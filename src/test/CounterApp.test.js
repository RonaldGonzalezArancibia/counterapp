import { shallow } from 'enzyme';
import CounterApp from './../CounterApp'

describe('test in customer app', () => {
    let wrapper = shallow(<CounterApp/>);
    beforeEach(() => {
        wrapper = shallow(<CounterApp/>);
    });
    
    test('should match whit snapshot', () => {
    expect(wrapper).toMatchSnapshot()
    })
    
    test('should show 20', () => {
        const number = 20;
        const wrapper = shallow(<CounterApp value={number}/>)
        const valueReturn = parseInt(wrapper.find('h2').text());
        expect(valueReturn).toBe(number);
    })

    test('should show default 100', () => {
        const number = 100;
        const valueReturn = parseInt(wrapper.find('h2').text());
        expect(valueReturn).toBe(number);
    });

    test('debe incrementar contador ', () => {
        wrapper.find('button').at(0).simulate('click');
        const valueReturn = parseInt(wrapper.find('h2').text());
        expect(valueReturn).toBe(101);    
    })
    
    test('debe decremento contador ', () => {
        wrapper.find('button').at(2).simulate('click');
        const valueReturn = parseInt(wrapper.find('h2').text());
        expect(valueReturn).toBe(99);   
    })

    test('debe restar contador ', () => {
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(1).simulate('click');
        const valueReturn = parseInt(wrapper.find('h2').text());
        expect(valueReturn).toBe(100);
    })
})
