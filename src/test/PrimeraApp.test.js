
import React from 'react';
// import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import PrimeraApp from "../PrimeraApp";

describe('pruebas de primeraApp', () => {

    // test('debe de mostrar el mensaje "hola soy goku" ', () => {
    //     const saludo = "hola soy goku";
    //     const { getByText } = render(<PrimeraApp saludo='hola soy goku' />);

    //     expect( getByText(saludo) ).toBeInTheDocument();
    // })

    test('debe de mostrar <primeraApp> correctamente ', () => {
        const saludo = "hola soy goku";
        const wrapper = shallow(<PrimeraApp saludo={saludo} />);

        expect(wrapper).toMatchSnapshot();
    })

    test('debe de mostrar el subtitulo enviado por props ', () => {
        const saludo = "hola soy goku";
        const subtitulo = 'hola mundo';
        const wrapper = shallow(
            <PrimeraApp
                saludo={saludo}
                subtitulo ={subtitulo}
            />);
        const testParrafo = wrapper.find('p').text();
        expect(testParrafo).toBe(subtitulo);

    })

})
