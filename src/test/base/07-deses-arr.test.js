import { retornaArreglo } from './../../base/07-deses-arr';


describe('pruebas de desectructuracion', () => {

    test('debe retornar un string y un numero', () => {

        const responseExpected =  ['ABC', 123];

        const respuesta = retornaArreglo();

        expect(respuesta).toEqual(responseExpected);

    })
})