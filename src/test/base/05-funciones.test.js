import { getUser, getUsuarioActivo } from './../../base/05-funciones';

describe('pruebas de funciones ', () =>{

    test('getUser', () => {
        const usertest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
    
        const user = getUser();
    
        expect(usertest).toEqual(user);
    })

    test('getUsuarioActivo', () => {
        const nombre= 'ronald';
        const userActiveTest = {
            uid: 'ABC567',
            username: nombre
        }
    
        const userActive = getUsuarioActivo(nombre);
    
        expect(userActive).toEqual(userActiveTest);
    })
})
