import { getHeroeById, getHeroesByOwner } from './../../base/08-imp-exp';

describe('pruebas en funciones de heroes', () => {

    test('debe retornar un heroe por ide', () => {

        const id = 1;
        const responseExpected =  {
            id: 1,
            name: 'Batman',
            owner: 'DC'
        };

        const heroe = getHeroeById(id);

        expect(heroe).toEqual(responseExpected);
    })

    test('debe retornar un heroe por ide que no existe', () => {

        const id = 10;

        const heroe = getHeroeById(id);

        expect(heroe).toBeUndefined();


    })

    test('debe retornar un heroe por owner DC', () => {

        const owner = 'DC';
        const respuestaEsperada= [
            {
                id: 1,
                name: 'Batman',
                owner: 'DC'
            },
            {
                id: 3,
                name: 'Superman',
                owner: 'DC'
            },
            {
                id: 4,
                name: 'Flash',
                owner: 'DC'
            }
        ];

        const heroes = getHeroesByOwner(owner);

        expect(heroes).toEqual(respuestaEsperada);
    })

    test('debe retornar un heroe por owner Marvel', () => {

        const owner = 'Marvel';
        const respuestaEsperada= [
            {
                id: 2,
                name: 'Spiderman',
                owner: 'Marvel'
            },
            {
                id: 5,
                name: 'Wolverine',
                owner: 'Marvel'
            },
        ];

        const heroes = getHeroesByOwner(owner);

        expect(heroes).toEqual(respuestaEsperada);
        expect(heroes.length).toBe(2);
    })
})