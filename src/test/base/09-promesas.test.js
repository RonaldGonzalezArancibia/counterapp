import { getHeroeByIdAsync } from './../../base/09-promesas';
describe('pruebas con promesas', () => {
    test('heroes async ', (done) => {
        const id = 1;
        const responseExpected = {
            id: 1,
            name: 'Batman',
            owner: 'DC'
        }
        getHeroeByIdAsync(id)
            .then(heroe => {
                expect(heroe).toEqual(responseExpected);
                done();
            })

    })

    test('heroes async return error ', (done) => {
        const id = 10;

        getHeroeByIdAsync(id)
            .catch(error =>{
                expect(error).toBe('No se pudo encontrar el héroe' );
                done();
            })

    })


})
