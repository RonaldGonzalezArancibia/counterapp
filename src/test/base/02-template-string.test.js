import '@testing-library/jest-dom';
import { getSaludo } from '../../base/02-template-string';
describe('pruebas en 02 template string', () => {

    test('prueba en el metodo getSaludo', () => {
        const nombre = 'ronald';
        const respunesta = getSaludo(nombre);

        expect(respunesta).toBe('Hola ronald');
    })

    test('prueba en el metodo getSaludo return default hola carlos', () => {
        const respunesta = getSaludo();

        expect(respunesta).toBe('Hola carlos');
    })
})