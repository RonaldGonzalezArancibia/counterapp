import { getImagen} from './../../base/11-async-await'

describe('pruebas con async await', () => {

    test('debe retornar url ', async () => {

        const url = await getImagen();

        console.log(url);
        expect(typeof url).toBe('string');
    })
    
})
